FROM alpine:latest

ARG ISTIO_VERSION
ARG KUBE_VERSION
ARG HELM_VERSION

ENV ISTIOCTL_VERSION=${ISTIO_VERSION} \
    KUBECTL_VERSION=${KUBE_VERSION} \
    HELMCTL_VERSION=${HELM_VERSION}

RUN apk update && apk upgrade && apk add --no-cache curl bash gettext tar && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

# Install Istioctl
RUN curl -sL https://github.com/istio/istio/releases/download/${ISTIOCTL_VERSION}/istioctl-${ISTIOCTL_VERSION}-linux-amd64.tar.gz | tar xvz && \
    chmod u+x istioctl && \
    mv istioctl /usr/local/bin/istioctl-${ISTIOCTL_VERSION} && \
    # Add Istio User
    adduser -D istio && \
    chown istio:istio /usr/local/bin/istioctl*
        
# Install Kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod u+x kubectl && \
    mv kubectl /usr/local/bin/kubectl && \
    chown istio:istio /usr/local/bin/kubectl

# Install Helm
RUN curl -sL https://get.helm.sh/helm-${HELMCTL_VERSION}-linux-amd64.tar.gz | tar xvz && \
    chmod u+x linux-amd64/helm && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    chown istio:istio /usr/local/bin/helm

USER istio